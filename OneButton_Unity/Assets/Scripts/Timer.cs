using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 
public class Timer : MonoBehaviour
{
    public float timeStart = 0;
    public TextMeshProUGUI textBox;
    public TextMeshProUGUI highScore;

    // Start is called before the first frame update
    void Start()
    {
        textBox.text = "Your Time: " + timeStart.ToString("F2");
        highScore.text = "HighScore: " + PlayerPrefs.GetFloat("HighScore", 0).ToString("F2");
    }

    // Update is called once per frame
    void Update()
    {
        timeStart += Time.deltaTime;
        
        textBox.text = "Your Time: " + timeStart.ToString("F2");

        if(timeStart > PlayerPrefs.GetFloat("HighScore", 0))
        {
            PlayerPrefs.SetFloat("HighScore", timeStart);
            highScore.text = "Your Time: " + timeStart.ToString("F2");
        }

        
    }
}
